### Pendo Extractor

This extractor queries the Pendo API for the following data sets:

* Aggregation Data - https://developers.pendo.io/docs/?python#aggregation
    * Feature Events
    * Guide Events
    * Page Events
    * Poll Events
    * Track Events

The full JSON response is stored in a single row and column (JSONTEXT) with the insertion timestamp in a separate column (UPLOADED_AT) in a Snowflake table in `raw.pendo.<object_name>`.

The dag that runs the job is in `/dags/pendo_extract.py`


#### Create Table Command

```sql
create or replace table pendo.featureevents (
jsontext variant,
uploaded_at timestamp_ntz(9) default CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9))
);
```
