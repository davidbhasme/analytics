import os
from datetime import datetime
from typing import Dict, Any

import requests


class PendoAPI:
    """
    Initialized with an integration_key, and optionally a
    start_date(2019-01-01), and request_interval(dayRange).
    Currently just allows fetching from the aggregation api.
    Currently returns a JSON datatype.
    """

    def __init__(
        self,
        datatype="JSON",
        endpoint="aggregation",
        integration_key=None,
        request_interval="dayRange",
        start_date=datetime(2019, 1, 1),
        timeout=120
            ):
        if endpoint == "aggregation":
            self.endpoint = endpoint
        else:
            raise Exception(
                "Only the aggregation endpoint is currently supported")

        self.base_url = f"https://app.pendo.io/api/v1/"
        self.start_date = start_date
        self.request_interval = request_interval

        if integration_key is not None:
            self.integration_key = integration_key
        else:
            self.integration_key = os.environ.get("PENDO_API_INTEGRATION_KEY")
        if self.integration_key is None:
            raise Exception("Pendo integration key not configured")

        # JSON or CSV
        self.datatype = datatype

        # Global headers
        self.headers = {}
        self.headers.update({"x-pendo-integration-key": self.integration_key})

        self.timeout = timeout

        if self.datatype == "JSON":
            self.headers.update({"content-type": "application/json"})

    def get_aggregation(self, source_name) -> Dict[Any, Any]:
        """
        API method to retrieve aggregation data for a given source_name.
        For a list of available sources, see
        https://developers.pendo.io/docs/?python#events-ungrouped
        """
        url = self.base_url + "aggregation"

        first_date = self.start_date.timestamp() * 1000
        data = "{\"response\":{\
                \"mimeType\":\"application/json\"},\
                \"request\":{\"pipeline\":[{\"source\":{\
                    \"" + source_name + "\":null,\
                    \"timeSeries\":{\
                        \"first\":\"" + str(first_date) + "+24*60*60*1000\",\
                        \"count\":1,\
                        \"period\":\"{" + self.request_interval + "}\"\
            }}}]}}"

        r = requests.post(
            url, timeout=self.timeout, headers=self.headers, data=data
            )

        r.raise_for_status()

        extract = r.json()
        aggregation_results = extract["results"]

        return aggregation_results
