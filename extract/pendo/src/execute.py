import json
import logging
import sys
from os import environ as env

from gitlabdata.orchestration_utils import (
    snowflake_engine_factory,
    snowflake_stage_load_copy_remove,
)

from api import PendoAPI


if __name__ == "__main__":

    logging.basicConfig(stream=sys.stout, level=20)

    pendo = PendoAPI()

    config_dict = env.copy()
    snowflake_engine = snowflake_engine_factory(config_dict, "LOADER")

    # Store Feature and Page Events
    sources = [
        "featureEvents",
        "guideEvents",
        "pollEvents",
        "pageEvents",
        "trackEvents"
        ]

    for source in sources:
        aggregation = pendo.get_aggregation(source)

        with open(f"{source}.json", "w") as outfile:
            json.dump(aggregation, outfile)

        snowflake_stage_load_copy_remove(
            f"{source}.json",
            "raw.pendo.pendo_load",
            f"raw.pendo.{source}",
            snowflake_engine,
        )
